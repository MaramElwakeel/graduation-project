function [ Accuracy ] = SVM( Training, TrainingLabels,  Testing, TestingLabels)

Network = fitcsvm(Training, TrainingLabels);
Result = predict(Network, Training);

%% ACCURACY %%
CP = classperf(TrainingLabels, Result);
Accuracy = get(CP, 'CorrectRate') * 100;

end