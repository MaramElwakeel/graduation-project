function  PlotSignals( Signals, Set )

for i = 1:size(Signals, 1)
    f = figure('visible', 'off')
    plot(Signals(i,:));
    title('Normal Signal');
    saveas(f, strcat(Set, num2str(i), ' (Normal).jpg'));
end %End of i
end

