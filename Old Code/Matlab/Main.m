%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%% Filtration: 1] Subbands = true: 5 Subbands(Delta, Theta, Alpha, Beta, Gamma) %%
%%              2] Subbands = false: Divide each signal to 4 signals each 1024   %%
%% Feature Extraction: Entropy or Wavelet + Entropy                              %%
%% Classification: SVM or PNN                                                    %%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%

%% INTIALIZATION %%
Subbands = false;
Extraction = 'Entropy';
Classifier = 'SVM';
TotalAccuracy = 0;

for i=1:5
    TestingFolder = i;
    %% READ SIGNALS %%
    TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'Z', TestingFolder, true )];
    TestingSignals = [ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'Z', TestingFolder, false )];
    
    %% FILTER SIGNALS %%
    FilteredTrainingSignals = FilterSignals(TrainingSignals, Subbands);
    FilteredTestingSignals = FilterSignals(TestingSignals, Subbands);
    
    %% FEATURE EXTRACTION %%
    if(strcmp(Extraction,'Entropy'))
        Training = Entropy(FilteredTrainingSignals, Subbands);
        Testing = Entropy(FilteredTestingSignals, Subbands);
    elseif(strcmp(Extraction,'Wavelet'))
        Training = WaveletTransform(FilteredTrainingSignals, Subbands);
        Testing = WaveletTransform(FilteredTestingSignals, Subbands);
    end;
      
    %% LABELS "CLASSES" %%%
    if(Subbands == true)
        TrainingLabels = horzcat(ones(1, length(Training) / 2), repmat(2, 1, length(Training) / 2));
        TestingLabels = horzcat(ones(1, length(Testing) / 2), repmat(2, 1, length(Testing) / 2));
        TrainingLabels = TrainingLabels';
        
    else
        FileID = fopen('TrainLabels.txt', 'r');
        TrainingLabels = fscanf(FileID, '%d %f');
        fclose(FileID);
        
        FileID = fopen('TestLabels.txt', 'r');
        TestingLabels = fscanf(FileID, '%d %f', [1,Inf]);
        fclose(FileID);
    end;
    
    %% CLASSIFICATION %%
    if(strcmp(Classifier,'SVM'))
        Accuracy = SVM(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'PNN'))
        Accuracy = PNN(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'LDA'))
        Accuracy = LDA(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'MLR'))
        Accuracy = MLR(Training, TrainingLabels, Testing, TestingLabels)
    end;
    TotalAccuracy = TotalAccuracy + Accuracy;
    
end; %End of for
TotalAccuracy = TotalAccuracy/5
