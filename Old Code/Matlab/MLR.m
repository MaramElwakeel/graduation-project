function [ Accuracy ] = MLR( Training, TrainingLabels,  Testing, TestingLabels)

Network = mnrfit(Training, TrainingLabels);
Result = mnrval(Network, Testing);

Correct = 0;
for i=1:length(Result)
    if((Result(i, 1) > Result(i, 2) && TestingLabels(i) == 1) || (Result(i, 1) < Result(i, 2) && TestingLabels(i) == 2))
        Correct = Correct + 1;
    end;
end;

%% ACCURACY %%
Accuracy = Correct / length(TestingLabels) * 100;

end