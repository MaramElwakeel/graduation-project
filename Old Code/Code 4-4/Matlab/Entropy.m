function [Features]  = Entropy( FilteredSignals, Subbands)

if(Subbands == true)
    limit = 3;
else
    limit = 2;
end;

for i = 1:size(FilteredSignals, limit)
    SD = std(FilteredSignals(:,i));
    
    if(Subbands == true)
        for j = 1:size(FilteredSignals, 2)
            ExtractedFeatures(i, j) = ApEn(3, 0.2 * SD, FilteredSignals(:,j,i), 0);
        end; %End of j
        
    else
        for j = 1:4
            NewSignal = FilteredSignals((1+(j-1)*1024 : 1024*j),i);
            ExtractedFeatures(i, j) = ApEn(3, 0.2 * SD, NewSignal, 0);
        end;
    end;
end; %End of i

if(Subbands == true)
    Features = ExtractedFeatures;
else
    Features = vertcat(ExtractedFeatures(:,1), ExtractedFeatures(:,2), ExtractedFeatures(:,3), ExtractedFeatures(:,4));
end;
end

