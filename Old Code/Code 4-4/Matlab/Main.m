%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%% Filtration: 1] Subbands = true: 5 Subbands(Delta, Theta, Alpha, Beta, Gamma) %%
%%              2] Subbands = false: Divide each signal to 4 signals each 1024   %%
%% Feature Extraction: Entropy or Wavelet + Entropy                              %%
%% Classification: SVM or PNN                                                    %%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%

%% INTIALIZATION %%
Subbands = false;
Extraction = 'Entropy';
Classifier = 'MLR';
TotalAccuracy = 0;
Classes = 2;
DividedClasses = true;
SeizureOREpileptic = 'Epileptic';

for i=1:5
    TestingFolder = i;
    %% READ SIGNALS %%
    
    if(strcmp(SeizureOREpileptic, 'Epileptic'))
        TrainingSignals = [ReadSignals( 'Z', TestingFolder, true ); ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
        TestingSignals = [ReadSignals( 'Z', TestingFolder, false ); ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'N', TestingFolder, false )];

    elseif(Classes == 3 || DividedClasses == true)
        TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'Z', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
        TestingSignals = [ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'Z', TestingFolder, false ); ReadSignals( 'N', TestingFolder, false )];
    else
        TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'Z', TestingFolder, true )];
        TestingSignals = [ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'Z', TestingFolder, false )];
        
    end;
    %% FILTER SIGNALS %%
    FilteredTrainingSignals = FilterSignals(TrainingSignals, Subbands);
    FilteredTestingSignals = FilterSignals(TestingSignals, Subbands);
    
    %% FEATURE EXTRACTION %%
    if(strcmp(Extraction,'Entropy'))
        Training = Entropy(FilteredTrainingSignals, Subbands);
        Testing = Entropy(FilteredTestingSignals, Subbands);
    elseif(strcmp(Extraction,'Wavelet'))
        Training = WaveletTransform(FilteredTrainingSignals, Subbands);
        Testing = WaveletTransform(FilteredTestingSignals, Subbands);
    end;
      
    %% LABELS "CLASSES" %%%
    if(Subbands == true)
        TrainingLabels = horzcat(ones(1, length(Training) / 2), repmat(2, 1, length(Training) / 2));
        TestingLabels = horzcat(ones(1, length(Testing) / 2), repmat(2, 1, length(Testing) / 2));
        TrainingLabels = TrainingLabels';
        
    else
        if(DividedClasses == true)
            TrainFileName = 'DividedClassesTrainLabelsSeizure.txt';
            TestFileName = 'DividedClassesTestLabelsSeizure.txt';
        elseif(Classes == 2)
            TrainFileName = '2ClassesTrainLabels.txt';
            TestFileName = '2ClassesTestLabels.txt';
        else
            TrainFileName = '3ClassesTrainLabels.txt';
            TestFileName = '3ClassesTestLabels.txt';
        end;
        
        FileID = fopen(TrainFileName, 'r');
        TrainingLabels = fscanf(FileID, '%d %f');
        fclose(FileID);
        
        FileID = fopen(TestFileName, 'r');
        TestingLabels = fscanf(FileID, '%d %f', [1,Inf]);
        fclose(FileID);
    end;
    
    %% CLASSIFICATION %%
    if(strcmp(Classifier,'SVM'))
        Accuracy = SVM(Training, TrainingLabels, Testing, TestingLabels, Classes)
    elseif(strcmp(Classifier,'PNN'))
        Accuracy = PNN(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'LDA'))
        Accuracy = LDA(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'MLR'))
        [ Accuracy, CorrectlyClassified, Labels ] = MLR(Training, TrainingLabels, Testing, TestingLabels, Classes, DividedClasses);
    end;
    
    if(DividedClasses == true)
        ClassesAccuracy = TrainAgain( CorrectlyClassified, Labels, SeizureOREpileptic, Classifier, TestingFolder );
         ClassesAccuracy = ClassesAccuracy / (length(TestingLabels) / 3) * 100;
        Accuracy = Accuracy + ClassesAccuracy
    end;
    TotalAccuracy = TotalAccuracy + Accuracy;
    
end; %End of for
TotalAccuracy = TotalAccuracy/5
