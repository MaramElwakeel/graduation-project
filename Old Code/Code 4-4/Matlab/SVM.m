function [ Accuracy ] = SVM( Training, TrainingLabels,  Testing, TestingLabels, Classes)

if(Classes == 2)
    Network = fitcsvm(Training, TrainingLabels);
else
    Network = fitcecoc(Training, TrainingLabels);
end;
Result = predict(Network, Testing);

%% ACCURACY %%
CP = classperf(TestingLabels, Result);
Accuracy = get(CP, 'CorrectRate') * 100;

end