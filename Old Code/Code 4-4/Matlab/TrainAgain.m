function [ Accuracy ] = TrainAgain( Testing, TestingLabels, SeizureOREpileptic, Classifier, TestingFolder )

%% READ SIGNALS %%
if(strcmp(SeizureOREpileptic, 'Seizure'))
    TrainingSignals = [ReadSignals( 'Z', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
    FileName = 'NormalInterictalTrain.txt';
else
    TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
    FileName = 'NormalInterictalTrain.txt';
end;

%% FILTER SIGNALS %%
FilteredTrainingSignals = FilterSignals(TrainingSignals, false);

%% FEATURE EXTRACTION %%
Training = Entropy(FilteredTrainingSignals, false);

%% LABELS "CLASSES" %%
FileID = fopen(FileName, 'r');
TrainingLabels = fscanf(FileID, '%d %f');
fclose(FileID);

%% CLASSIFICATION %%
if(strcmp(Classifier,'SVM'))
    [ Accuracy, CorrectlyClassified, Labels] = SVM(Training, TrainingLabels, Testing, TestingLabels, 2);
elseif(strcmp(Classifier,'MLR'))
    [ Accuracy,CorrectlyClassified, Labels] = MLR(Training, TrainingLabels, Testing', TestingLabels, 2, false);
     Accuracy = (Accuracy / 100) * (length(TestingLabels) / 3);
end;

end

