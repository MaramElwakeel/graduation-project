function [FinalSignals] = ReadSignals( Set, TestingFolder, Training)


Folders = 5;
k = 1;
Files = 0;

if(Training == false)
    Folders = 1;
end;

for f = 1:Folders
    if(Training == true && f ~= TestingFolder)
        Files = dir(fullfile(strcat('D:\FCIS\4\GP\Codes\Code 14-3\Matlab\DataSet\', Set , '\', num2str(f)), strcat(Set, '*.txt')));
    elseif (Training == false)
        Files = dir(fullfile(strcat('D:\FCIS\4\GP\Codes\Code 14-3\Matlab\DataSet\', Set , '\', num2str(TestingFolder)), strcat(Set, '*.txt')));
    else
        Files = 0;
    end
    
    if(length(Files) ~= 1)
        for i = 1:length(Files);
            FileName = Files(i).name;
            FileID = fopen(FileName, 'r');
            formatSpec = '%d %f';
            sizeMatrix = [1, Inf];
            Signal = fscanf(FileID, formatSpec, sizeMatrix);
            fclose(FileID);
            
            Min = min(Signal);
            Max = max(Signal);
            
            %GET NORMALIZED VALUES OF A SIGNAL.
            for j = 1:length(Signal);
                NormalizedSignal(j) = (2 * (Signal(j) - Min) / (Max - Min)) - 1;
            end %End of j
                      
            FinalSignals(k,:) = NormalizedSignal;
            k = k + 1;
        end %End of i
    end %End of if
end %End of f
end


