% %%%%SVM + ENTROPY + WAVELET%%%
% TrainingSignals = [ReadSignals( 'S', 5, true ); ReadSignals( 'Z', 5, true )];
% 
% % PlotSignals(Signals, 'Z');
% 
% FilteredTrainingSignals = FilterSignalsSubbands(TrainingSignals);
% 
% %FeaturesTrainingSignals = Entropy(FilteredTrainingSignals);
% FeaturesTrainingSignals = WaveletTransform(FilteredTrainingSignals);
% 
% %%%%TESTING%%%%
% TestingSignals = [ReadSignals( 'S', 5, false ); ReadSignals( 'Z', 5, false )];
% FilteredTestingSignals = FilterSignalsSubbands(TestingSignals);
% % FeaturesTestingSignals = Entropy(FilteredTestingSignals);
% FeaturesTestingSignals = WaveletTransform(FilteredTestingSignals);
% 
% TrainingLabels = horzcat(ones(1, length(FeaturesTrainingSignals) / 2), repmat(2, 1, length(FeaturesTrainingSignals) / 2));
% TesitngLabels = vertcat(ones(length(FeaturesTestingSignals) / 2, 1), repmat(2, length(FeaturesTestingSignals) / 2, 1));
% Accuracy = SVM(FeaturesTrainingSignals, TrainingLabels, FeaturesTestingSignals, TesitngLabels)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% % %%% ENTROPY + PNN %%%
% TrainingSignals = [ReadSignals( 'S', 2, true ); ReadSignals( 'Z', 2, true )];
% FilteredTrainingSignals = FilterSignals(TrainingSignals);
% FeaturesTrainingSignals = Entropy(FilteredTrainingSignals);
% 
% %%%%TESTING%%%%
% TestingSignals = [ReadSignals( 'S', 2, false ); ReadSignals( 'Z', 2, false )];
% FilteredTestingSignals = FilterSignals(TestingSignals);
% FeaturesTestingSignals = Entropy(FilteredTestingSignals);
% 
% Train = vertcat(FeaturesTrainingSignals(:,1), FeaturesTrainingSignals(:,2), FeaturesTrainingSignals(:,3), FeaturesTrainingSignals(:,4));
% Test = vertcat(FeaturesTestingSignals(:,1), FeaturesTestingSignals(:,2), FeaturesTestingSignals(:,3), FeaturesTestingSignals(:,4));
% 
% FileID = fopen('TrainLabels.txt', 'r');
% TrainingLabels = fscanf(FileID, '%d %f');
% fclose(FileID);
% 
% FileID = fopen('TestLabels.txt', 'r');
% TestingLabels = fscanf(FileID, '%d %f', [1,Inf]);
% fclose(FileID);
%  
% Accuracy = PNN(Train, TrainingLabels, Test, TestingLabels)
