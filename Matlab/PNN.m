function [ Accuracy ] = PNN( Training, TrainingLabels,  Testing, TestingLabels)

Network = newpnn(Training', ind2vec(TrainingLabels'), 0.1);
Result = sim(Network, Testing');

%% ACCURACY %%
CP = classperf(ind2vec(TestingLabels), Result);
Accuracy = get(CP, 'CorrectRate') * 100;

end