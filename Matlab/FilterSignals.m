function [FilteredSignals] = FilterSignals(Signals, Subbands)

fmax = 173.61/2;

for i = 1:size(Signals, 1)
    if(Subbands == false)
        [b, a] = butter(5, [0.53/fmax, 40/fmax],'bandpass');
        FilteredSignals(:,i) = filter(b, a, Signals(i,:));
        
    else
        SubBandsNames = {'Delta', 'Theta', 'Alpha', 'Beta', 'Gamma'};
        SubBandStart = [1, 4, 8, 16, 32];
        SubBandEnd = [4, 8, 16, 32, 64];
        
        for j = 1:length(SubBandsNames)
            [b, a] = butter(5, [SubBandStart(j)/fmax, SubBandEnd(j)/fmax],'bandpass');
            FilteredSignals(:,j,i) = filter(b, a, Signals(i,:));
        end; %End of j
    end; %End of if
end %End of i

end %End of function.

