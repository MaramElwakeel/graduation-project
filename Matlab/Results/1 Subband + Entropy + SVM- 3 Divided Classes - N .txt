Normal VS (Ictal and Interictal)

1] Testing Folder: 1
    Accuracy =  71.25%
    
    CorrectNormal = 79
    CorrectInterictal = 42
    CorrectIctal = 50

2] Testing Folder: 2
    Accuracy = 69.5833%

    CorrectNormal = 80
    CorrectInterictal = 29
    CorrectIctal = 58

3] Testing Folder: 3
    Accuracy = 73.3333%

    CorrectNormal = 77
    CorrectInterictal = 56
    CorrectIctal = 43

4] Testing Folder: 4
    Accuracy = 73.75%

    CorrectNormal = 79
    CorrectInterictal = 32
    CorrectIctal = 66

5] Testing Folder: 5
    Accuracy = 70.8333%

    CorrectNormal = 80
    CorrectInterictal = 34
    CorrectIctal = 56

--------------------------------------------------

Accuracy = Total / 5 =  71.75%


////////////////////////////////////////////////////////////////////////////////////

Ictal VS (Normal and Interictal)

1] Testing Folder: 1
    Accuracy =  71.6667%

    CorrectIctal = 47
    CorrectInterictal = 46
    CorrectNormal = 79

2] Testing Folder: 2
    Accuracy = 70.4167%

    CorrectIctal = 55
    CorrectInterictal = 80
    CorrectNormal = 34

3] Testing Folder: 3
    Accuracy = 72.9167%

    CorrectIctal = 51
    CorrectInterictal = 44
    CorrectNormal = 80

4] Testing Folder: 4
    Accuracy = 72.5%

    CorrectIctal = 63
    CorrectInterictal = 32
    CorrectNormal = 79

5] Testing Folder: 5
    Accuracy = 69.5833%

    CorrectIctal = 54
    CorrectInterictal = 33
    CorrectNormal = 80

--------------------------------------------------

Accuracy = Total / 5 = 71.4167%