function [ Correct, CorrectlyClassified, Labels ] = MLR( Training, TrainingLabels,  Testing, TestingLabels, Classes, DividedClasses)

Network = mnrfit(Training, TrainingLabels);
Result = mnrval(Network, Testing);

Correct = 0;
FirstClassCounter = 1;
CorrectlyClassified = 0;
Labels = 0;

for i=1:length(Result)
    if(Classes == 2)
        if((Result(i, 1) > Result(i, 2) && TestingLabels(i) == 1) || (Result(i, 1) < Result(i, 2) && TestingLabels(i) == 2))
            Correct = Correct + 1;
            if(TestingLabels(i) == 1)
                CorrectlyClassified(FirstClassCounter) = Testing(i);
                if((i >= 21 && i <= 40) || (i >= 81 && i <= 100) || (i >= 141 && i <= 160) || (i >= 201 && i <= 220))
                    Labels(FirstClassCounter) = 2;
                else
                    Labels(FirstClassCounter) = 1;
                end;
                FirstClassCounter = FirstClassCounter + 1;
            end;
        end;
        
    else %3 Classes
        if((Result(i, 1) > Result(i, 2) && Result(i, 1) > Result(i, 3) && TestingLabels(i) == 1) || (Result(i, 1) < Result(i, 2) && Result(i, 3) < Result(i, 2) && TestingLabels(i) == 2) || (Result(i, 1) < Result(i, 3) && Result(i, 2) < Result(i, 3) && TestingLabels(i) == 3))
            Correct = Correct + 1;
        end;
     end;
end;

%% ACCURACY %%
if(DividedClasses == true)
    Correct = Correct - length(CorrectlyClassified);
%     Accuracy = Correct / (length(TestingLabels) / 3) * 100;
% else
%     Accuracy = Correct / length(TestingLabels) * 100;
end;


end