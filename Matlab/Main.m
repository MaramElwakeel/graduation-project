%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
%% Filtration: 1] Subbands = true: 5 Subbands(Delta, Theta, Alpha, Beta, Gamma) %%
%%              2] Subbands = false: Divide each signal to 4 signals each 1024   %%
%% Feature Extraction: Entropy or Wavelet + Entropy                              %%
%% Classification: SVM or PNN                                                    %%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%

%% INTIALIZATION %%
Subbands = false;
Extraction = 'Entropy';
Classifier = 'SVM';
TotalAccuracy = 0;
Classes = 2;
DividedClasses = true;
SeizureOREpileptic = 'Epileptic';

% for i=1:5
    TestingFolder = 5;
    %% READ SIGNALS %%
    
    if(strcmp(SeizureOREpileptic, 'Epileptic'))
        TrainingSignals = [ReadSignals( 'Z', TestingFolder, true ); ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
        TestingSignals = [ReadSignals( 'Z', TestingFolder, false ); ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'N', TestingFolder, false )];
        
    elseif(Classes == 3 || DividedClasses == true)
        TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'Z', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
        TestingSignals = [ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'Z', TestingFolder, false ); ReadSignals( 'N', TestingFolder, false )];
    else
        TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'Z', TestingFolder, true )];
        TestingSignals = [ReadSignals( 'S', TestingFolder, false ); ReadSignals( 'Z', TestingFolder, false )];
        
    end;
    %% FILTER SIGNALS %%
    FilteredTrainingSignals = FilterSignals(TrainingSignals, Subbands);
    FilteredTestingSignals = FilterSignals(TestingSignals, Subbands);
    
    %% FEATURE EXTRACTION %%
    if(strcmp(Extraction,'Entropy'))
        Training = Entropy(FilteredTrainingSignals, Subbands);
        Testing = Entropy(FilteredTestingSignals, Subbands);
    elseif(strcmp(Extraction,'Wavelet'))
        Training = WaveletTransform(FilteredTrainingSignals, Subbands);
        Testing = WaveletTransform(FilteredTestingSignals, Subbands);
    end;
    
    %% LABELS "CLASSES" %%%
    if(Subbands == true)
        TrainingLabels = horzcat(ones(1, length(Training) / 2), repmat(2, 1, length(Training) / 2));
        TestingLabels = horzcat(ones(1, length(Testing) / 2), repmat(2, 1, length(Testing) / 2));
        TrainingLabels = TrainingLabels';
        
    else
        if(DividedClasses == true)
            TrainFileName = 'DividedClassesTrainLabelsSeizure.txt';
            TestFileName = 'DividedClassesTestLabelsSeizure.txt';
        elseif(Classes == 2)
            TrainFileName = '2ClassesTrainLabels.txt';
            TestFileName = '2ClassesTestLabels.txt';
        else
            TrainFileName = '3ClassesTrainLabels.txt';
            TestFileName = '3ClassesTestLabels.txt';
        end;
        
        FileID = fopen(TrainFileName, 'r');
        TrainingLabels = fscanf(FileID, '%d %f');
        fclose(FileID);
        
        FileID = fopen(TestFileName, 'r');
        TestingLabels = fscanf(FileID, '%d %f', [1,Inf]);
        fclose(FileID);
    end;
    
    %% CLASSIFICATION %%
    if(strcmp(Classifier,'SVM'))
        [ Correct, CorrectlyClassified, Labels ] = SVM(Training, TrainingLabels, Testing, TestingLabels, Classes, DividedClasses);
    elseif(strcmp(Classifier,'PNN'))
        Accuracy = PNN(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'LDA'))
        Accuracy = LDA(Training, TrainingLabels, Testing, TestingLabels)
    elseif(strcmp(Classifier,'MLR'))
        [ Correct, CorrectlyClassified, Labels ] = MLR(Training, TrainingLabels, Testing, TestingLabels, Classes, DividedClasses);
    end;
    
    if(DividedClasses == true)
        Accuracy = Correct / (length(TestingLabels)/3) * 100;
        [Correct1, Correct2] = TrainAgain( CorrectlyClassified, Labels, SeizureOREpileptic, Classifier, TestingFolder );
        Correct
        Correct1
        Correct2
        Accuracy1 = Correct1 / (length(TestingLabels) / 3) * 100;
        Accuracy2 = Correct2 / (length(TestingLabels) / 3) * 100;
        Accuracy = (Accuracy + Accuracy1 + Accuracy2)/3
    else
        Accuracy =  Correct / length(TestingLabels) * 100
    end;
    TotalAccuracy = TotalAccuracy + Accuracy;
    
% end; %End of for
TotalAccuracy = TotalAccuracy/5
