function [ Correct, CorrectlyClassified, Labels ] = SVM( Training, TrainingLabels,  Testing, TestingLabels, Classes, DividedClasses)

if(Classes == 2)
%     Network = fitcsvm(Training, TrainingLabels);
    Network = svmtrain(Training, TrainingLabels);
else
    Network = fitcecoc(Training, TrainingLabels);
end;
% Result = predict(Network, Testing);
Result = svmclassify(Network, Testing);

Correct = 0;
FirstClassCounter = 1;
CorrectlyClassified = 0;
Labels = 0;

if(Classes == 2)
    for i=1:length(Result)
        if(Result(i) == TestingLabels(i))
            Correct = Correct + 1;
            if(TestingLabels(i) == 1)
                CorrectlyClassified(FirstClassCounter) = Testing(i);
                if((i >= 21 && i <= 40) || (i >= 81 && i <= 100) || (i >= 141 && i <= 160) || (i >= 201 && i <= 220))
                    Labels(FirstClassCounter) = 2;
                else
                    Labels(FirstClassCounter) = 1;
                end;
                FirstClassCounter = FirstClassCounter + 1;
            end;
        end;
    end;
else
    CP = classperf(TestingLabels, Result);
    Correct = get(CP, 'CorrectRate') * length(TestingLabels);
end;


%% ACCURACY %%
if(DividedClasses == true)
    Correct = Correct - length(CorrectlyClassified);
end;

end