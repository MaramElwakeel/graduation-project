function [Features] = WaveletTransform( FilteredSignals, Subbands )

if(Subbands == true)
    limit = 3;
else
    limit = 2;
end;

for i = 1:size(FilteredSignals, limit)
    if(Subbands == true)
        Start = 1;
        for j = 1:size(FilteredSignals, 2)
            [Lo_D,Hi_D] = wavedec(FilteredSignals(:,j,i), 9, 'db4');
            End = Start + Hi_D(j) - 1;
            ExtractedFeatures(i, j, :) = Entropy(Lo_D(Start:End), Subbands);
            Start = End + 1;
        end; %End of j
        
    else
        SD = std(FilteredSignals(:,i));
        for j = 1:4
            NewSignal = FilteredSignals((1+(j-1)*1024 : 1024*j),i);
            [Lo_D,Hi_D] = wavedec(NewSignal, 4, 'db1');
            ExtractedFeatures(i, j) = ApEn(3, 0.2 * SD, Lo_D(1:Hi_D(1)), 0);
        end
    end;
end; %End of i

if(Subbands == true)
    Features = ExtractedFeatures;
else
    Features = vertcat(ExtractedFeatures(:,1), ExtractedFeatures(:,2), ExtractedFeatures(:,3), ExtractedFeatures(:,4));
end;
end

