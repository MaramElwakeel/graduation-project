function [ Accuracy ] = LDA( Training, TrainingLabels,  Testing, TestingLabels)

Network = fitcdiscr(Training, TrainingLabels);
Result = predict(Network, Testing);

%% ACCURACY %%
CP = classperf(TestingLabels', Result);
Accuracy = get(CP, 'CorrectRate') * 100;

end