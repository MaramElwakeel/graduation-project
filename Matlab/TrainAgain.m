function [ Correct1, Correct2 ] = TrainAgain( Testing, TestingLabels, SeizureOREpileptic, Classifier, TestingFolder )

%% READ SIGNALS %%
if(strcmp(SeizureOREpileptic, 'Seizure'))
    TrainingSignals = [ReadSignals( 'Z', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
    FileName = 'NormalInterictalTrain.txt';
else
    TrainingSignals = [ReadSignals( 'S', TestingFolder, true ); ReadSignals( 'N', TestingFolder, true )];
    FileName = 'NormalInterictalTrain.txt';
end;

%% FILTER SIGNALS %%
FilteredTrainingSignals = FilterSignals(TrainingSignals, false);

%% FEATURE EXTRACTION %%
Training = Entropy(FilteredTrainingSignals, false);

%% LABELS "CLASSES" %%
FileID = fopen(FileName, 'r');
TrainingLabels = fscanf(FileID, '%d %f');
fclose(FileID);

%% CLASSIFICATION %%
if(strcmp(Classifier,'SVM'))
    [ Correct, CorrectlyClassified, Labels] = SVM(Training, TrainingLabels, Testing', TestingLabels, 2, false);
elseif(strcmp(Classifier,'MLR'))
    [ Correct,CorrectlyClassified, Labels] = MLR(Training, TrainingLabels, Testing', TestingLabels, 2, false);
end;
Correct1 = Correct - length(CorrectlyClassified);
Correct2 = length(CorrectlyClassified);
end

