﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using System.Threading;

namespace GP
{
    public partial class MainForm : Form
    {
        const int SignalLength = 5000;
        int SignalLimit = 0, NormalizedSignalLimit = 0, FilteredSignalLimit = 0; 
        string Ouput = "";
        List<double> TestSignal, NormalizedSignal, Features;

        public MainForm()
        {
            InitializeComponent();
        }

        private void BrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog FileDialog = new OpenFileDialog();
            if (FileDialog.ShowDialog() == DialogResult.OK)
            {
                Reset();

                string SignalName = FileDialog.FileName;
                StreamReader SR = new StreamReader(SignalName);
                string Value;
                TestSignal = new List<double>(SignalLength);

                while ((Value = SR.ReadLine()) != null)
                    TestSignal.Add(double.Parse(Value));

                SR.Close();

                NormalizedSignal = Operations.NormalizeSignal(TestSignal);
                NormalizedSignalChart.Invalidate();

                List<double> FilteredSignal = Operations.FilterSignal(NormalizedSignal);

                Features = Operations.ExtractFeatures(FilteredSignal);
                //PlotFeatures(Features);

                List<ClassType> ClassificationResult = Operations.Classify(Features);
                //string Ouput = "";
                for (int i = 0; i < ClassificationResult.Count; i++)
                    if (i % 2 == 0)
                        Ouput += "Window " + (i + 1).ToString() + ": " + ClassificationResult[i].ToString() + "               ";
                    else
                        Ouput += "Window " + (i + 1).ToString() + ": " + ClassificationResult[i].ToString() + "\r\n";

                //ClassTextbox.Text = Ouput;
            }
        }

        private void Reset()
        {
            NormalizedSignalChart.Series[0].Points.Clear();
            FeaturesChart.Series[0].Points.Clear();

            SignalLimit = NormalizedSignalLimit = FilteredSignalLimit = 0;
            TestSignal = null;
            NormalizedSignal = null;

            ClassTextbox.Clear();
            Ouput = "";
        }

        private void PlotSignal(List<double> Values, int Limit)
        {
            NormalizedSignalChart.Series[0].Points.Clear();
            NormalizedSignalChart.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            NormalizedSignalChart.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;

            for (int i = 0; i <= Limit; i++)
                NormalizedSignalChart.Series[0].Points.AddY(Values[i]);

            NormalizedSignalChart.Invalidate();
        }

        private void PlotFeatures(List<double> Values)
        {
            FeaturesChart.Series[0].Points.Clear();
            FeaturesChart.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            FeaturesChart.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;

            for (int i = 0; i < Values.Count; i++)
                FeaturesChart.Series[0].Points.AddXY(i, Values[i]);
        }

        private void NormalizedSignalChart_Paint(object sender, PaintEventArgs e)
        {
            if (NormalizedSignal != null && NormalizedSignalLimit < NormalizedSignal.Count)
            {
                PlotSignal(NormalizedSignal, NormalizedSignalLimit);
                NormalizedSignalLimit++;
            }
            if (NormalizedSignal != null && Features!= null && NormalizedSignalLimit == NormalizedSignal.Count / 15)
                PlotFeatures(Features);
            if (NormalizedSignal != null && Ouput != "" && NormalizedSignalLimit == NormalizedSignal.Count / 10)
                ClassTextbox.Text = Ouput;
        }
    }
}
