﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GP
{
    public enum ClassType
    {
        Normal, Epileptic, Ictal, Interictal
    };

    public static class Operations
    {
        static public List<double> NormalizeSignal(List<double> Signal)
        {
            List<double> OutputSignal = new List<double>(Signal.Count);

            double Max = Signal.Max();
            double Min = Signal.Min();

            for (int i = 0; i < Signal.Count; i++)
                OutputSignal.Add((2.0 * (Signal[i] - Min) / (Max - Min)) - 1.0);

            return OutputSignal;
        }

        static public List<double> FilterSignal(List<double> Signal)
        {
            List<double> OutputSignal = new List<double>(Signal.Count);

            List<double> B = new List<double> { 0.0363534820525664, 0, -0.181767410262832, 0, 0.363534820525664, 0, -0.363534820525664, 0, 0.181767410262832, 0, -0.0363534820525664 };
            List<double> A = new List<double> { 1, -5.35448842539065, 12.5169436286371, -17.3370603670267, 16.4838197904024, -11.5631929743302, 5.92078744304580, -2.09978055488287, 0.514726637743414, -0.0859453368183458, 0.00419016181055992 };


            for (int n = 0; n < Signal.Count; n++)
            {
                double Y = 0.0;

                for (int k = 0; k < B.Count && k <= n; k++)
                {
                    if (n - k < Signal.Count)
                        Y += B[k] * Signal[n - k];
                }
                for (int k = 0; k < A.Count && k <= n; k++)
                {
                    if (n != 0 && n - k < OutputSignal.Count)
                        Y -= A[k] * OutputSignal[n - k];
                }
                OutputSignal.Add(Y);
            }
            return OutputSignal;
        }

        static public List<double> ExtractFeatures(List<double> Signal)
        {
            List<double> OutputSignal = new List<double>();
            double SD = HelperMethods.CalculatSTD(Signal);

            for (int i = 0; i < 4; i++)
            {
                List<double> SubBand = new List<double>();
                for (int j = 0; j < 1024; j++)
                    SubBand.Add(Signal[j + i * 1024]);
                OutputSignal.Add(ApEn(3, 0.2 * SD, SubBand));
            }
            return OutputSignal;
        }

        static public List<ClassType> Classify(List<double> Features)
        {
            List<double> F1 = new List<double>(Features);
            List<double> F2 = new List<double>(Features);
            List<ClassType> Windows = new List<ClassType>();

            for (int i = 0; i < Features.Count; i++)
            {
                if (SVM(F1[i], 0) == ClassType.Normal)
                    Windows.Add(ClassType.Normal);
                else
                    Windows.Add(SVM(F2[i], 1));
            }
            return Windows;
        }

        // private static ClassType SVM(List<double> Features, int Level)
        //{
        //    Network.Initialize(Level);

        //    //Normalize Data
        //    for (int i = 0; i < Features.Count; i++)
        //        Features[i] = Network.ScaleData.ScaleFactor * (Features[i] + Network.ScaleData.Shift);

        //    double Result = 0.0;
        //    for (int i = 0; i < Network.SupportVectors.Count; i++)
        //    {
        //        Result += Network.Alpha[i] * HelperMethods.DotProduct(Features, Network.SupportVectors[i]);
        //    }
        //    Result += Network.Bias;

        //    if(Level == 0)
        //    {
        //        if(Result >= 0)
        //            return ClassType.Epileptic;
        //        else
        //            return ClassType.Normal;
        //    }
        //    else
        //    {
        //        if(Result >= 0)
        //            return ClassType.Ictal;
        //        else
        //            return ClassType.Interictal;
        //    }
        //}

        private static ClassType SVM(double Signal, int Level)
        {
            Network.Initialize(Level);

            //Normalize Data
            Signal = Network.ScaleData.ScaleFactor * (Signal + Network.ScaleData.Shift);

            double Result = 0.0;

            for (int i = 0; i < Network.SupportVectors.Count; i++)
                Result += Network.Alpha[i] * (Signal * Network.SupportVectors[i]);

            Result += Network.Bias;

            if (Level == 0)
            {
                if (Result >= 0)
                    return ClassType.Epileptic;
                else
                    return ClassType.Normal;
            }
            else
            {
                if (Result >= 0)
                    return ClassType.Ictal;
                else
                    return ClassType.Interictal;
            }
        }

        static private double ApEn(int Dimension, double r, List<double> Data)
        {
            List<double> Result = new List<double>();
            for (int c = 0; c < 2; c++)
            {
                int m = Dimension + c;
                int N = Data.Count;

                // Fill Matrix.
                List<List<double>> DataMatrix = new List<List<double>>();
                HelperMethods.FillDataMatrix(N, m, Data, ref DataMatrix);

                // Operations.
                List<double> PhiList = new List<double>();
                for (int i = 0; i < N - m + 1; i++)
                {
                    double BoolVectorSum = 0, TempPhiSum = 0;

                    List<List<double>> RepeatedMatrix = new List<List<double>>();
                    for (int col = 0; col < m; col++)
                    {
                        List<double> sublist = new List<double>();
                        sublist.Add(DataMatrix[col][i]);
                        RepeatedMatrix.Add(sublist);
                    }
                    RepeatedMatrix = HelperMethods.Repmat(RepeatedMatrix, m, N - m + 1);

                    HelperMethods.CountingSimilarPatterns(DataMatrix, RepeatedMatrix, r, ref BoolVectorSum);
                    TempPhiSum = BoolVectorSum / (N - m + 1);
                    PhiList.Add(Math.Log(TempPhiSum));
                }
                double PhiSum = PhiList.Sum();
                Result.Add(PhiSum / (N - m + 1));
            } // End c

            double ApEn = 0;
            ApEn = Result[0] - Result[1];
            return ApEn;
        } // End ApEn
    }
}