﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace GP
{
    static public class HelperMethods
    {
        static public List<List<double>> FillDataMatrix(int N, int m, List<double> Data, ref List<List<double>> DataMatrix)
        {
            for (int i = 0; i < m; i++)
            {
                List<double> sublist = new List<double>();
                for (int j = i; j < N - m + i + 1; j++)
                {
                    sublist.Add(Data[j]);
                }
                DataMatrix.Add(sublist);
            }
            return DataMatrix;
        }

        static public void CountingSimilarPatterns(List<List<double>> DataMatrix, List<List<double>> RepeatedMatrix, double r, ref double BoolVectorSum)
        {
            double[] BoolVector = new double[DataMatrix[0].Count];

            for (int i = 0; i < DataMatrix.Count; i++)
            {
                for (int j = 0; j < DataMatrix[i].Count; j++)
                {
                    double TempData = Math.Abs(DataMatrix[i][j] - RepeatedMatrix[i][j]);
                    if (TempData > r)
                    {
                        BoolVector[j] = 1;
                    }
                }
            }
            BoolVectorSum = BoolVector.Length - BoolVector.Sum(); // Sum of Zeros
        }

        static public List<List<double>> Repmat(List<List<double>> Matrix, int Rows, int Counter)
        {
            double[] array;
            for (int i = 0; i < Rows; i++)
            {
                array = new double[Matrix[i].Count];
                array = Matrix[i].ToArray();
                for (int j = 0; j < Counter - 1; j++)
                {
                    Matrix[i].AddRange(array);
                }
            }
            return Matrix;
        }

        static public double CalculatSTD(List<double> Vector)
        {
            double average = Vector.Average();
            double sumOfSquaresOfDifferences = Vector.Select(val => (val - average) * (val - average)).Sum();
            double SD = Math.Sqrt(sumOfSquaresOfDifferences / Vector.Count);
            return SD;
        }

        static public double DotProduct(List<double> Features, double Value)
        {
            double Result = 0.0;
            for (int i = 0; i < Features.Count; i++)
                Result += Features[i] * Value;
            return Result;
        }
    }
}
