﻿namespace GP
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.BrowseButton = new System.Windows.Forms.Button();
            this.NormalizedSignalChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FeaturesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ClassTextbox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.NormalizedSignalChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeaturesChart)).BeginInit();
            this.SuspendLayout();
            // 
            // BrowseButton
            // 
            this.BrowseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BrowseButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BrowseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BrowseButton.Location = new System.Drawing.Point(32, 12);
            this.BrowseButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.BrowseButton.Name = "BrowseButton";
            this.BrowseButton.Size = new System.Drawing.Size(144, 40);
            this.BrowseButton.TabIndex = 0;
            this.BrowseButton.Text = "Browse Signal";
            this.BrowseButton.UseVisualStyleBackColor = true;
            this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
            // 
            // NormalizedSignalChart
            // 
            this.NormalizedSignalChart.BackColor = System.Drawing.Color.SteelBlue;
            this.NormalizedSignalChart.BorderlineColor = System.Drawing.Color.Black;
            chartArea1.BackColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            chartArea1.ShadowColor = System.Drawing.Color.Transparent;
            this.NormalizedSignalChart.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.SteelBlue;
            legend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            this.NormalizedSignalChart.Legends.Add(legend1);
            this.NormalizedSignalChart.Location = new System.Drawing.Point(32, 75);
            this.NormalizedSignalChart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.NormalizedSignalChart.Name = "NormalizedSignalChart";
            this.NormalizedSignalChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Grayscale;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.Black;
            series1.LabelBackColor = System.Drawing.Color.Black;
            series1.Legend = "Legend1";
            series1.Name = "Signal";
            this.NormalizedSignalChart.Series.Add(series1);
            this.NormalizedSignalChart.Size = new System.Drawing.Size(1246, 223);
            this.NormalizedSignalChart.TabIndex = 2;
            this.NormalizedSignalChart.Text = "Signal";
            this.NormalizedSignalChart.Paint += new System.Windows.Forms.PaintEventHandler(this.NormalizedSignalChart_Paint);
            // 
            // FeaturesChart
            // 
            this.FeaturesChart.BackColor = System.Drawing.Color.SteelBlue;
            this.FeaturesChart.BorderlineColor = System.Drawing.Color.Black;
            chartArea2.BackColor = System.Drawing.Color.Transparent;
            chartArea2.Name = "ChartArea1";
            chartArea2.ShadowColor = System.Drawing.Color.Transparent;
            this.FeaturesChart.ChartAreas.Add(chartArea2);
            legend2.BackColor = System.Drawing.Color.SteelBlue;
            legend2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend2.IsTextAutoFit = false;
            legend2.Name = "Legend1";
            this.FeaturesChart.Legends.Add(legend2);
            this.FeaturesChart.Location = new System.Drawing.Point(44, 304);
            this.FeaturesChart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.FeaturesChart.Name = "FeaturesChart";
            this.FeaturesChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Grayscale;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series2.Color = System.Drawing.Color.Black;
            series2.LabelBackColor = System.Drawing.Color.Black;
            series2.Legend = "Legend1";
            series2.Name = "Features";
            series2.YValuesPerPoint = 2;
            this.FeaturesChart.Series.Add(series2);
            this.FeaturesChart.Size = new System.Drawing.Size(1246, 223);
            this.FeaturesChart.TabIndex = 3;
            this.FeaturesChart.Text = "Signal";
            // 
            // ClassTextbox
            // 
            this.ClassTextbox.BackColor = System.Drawing.Color.SteelBlue;
            this.ClassTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ClassTextbox.Font = new System.Drawing.Font("Century Schoolbook", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassTextbox.ForeColor = System.Drawing.Color.Black;
            this.ClassTextbox.Location = new System.Drawing.Point(141, 549);
            this.ClassTextbox.Multiline = true;
            this.ClassTextbox.Name = "ClassTextbox";
            this.ClassTextbox.ReadOnly = true;
            this.ClassTextbox.Size = new System.Drawing.Size(958, 114);
            this.ClassTextbox.TabIndex = 4;
            this.ClassTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(1303, 675);
            this.Controls.Add(this.ClassTextbox);
            this.Controls.Add(this.FeaturesChart);
            this.Controls.Add(this.NormalizedSignalChart);
            this.Controls.Add(this.BrowseButton);
            this.Font = new System.Drawing.Font("Calisto MT", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Classification of EEG Signals for Detection of Epileptic Seizures";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.NormalizedSignalChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeaturesChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BrowseButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart NormalizedSignalChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart FeaturesChart;
        private System.Windows.Forms.TextBox ClassTextbox;
    }
}

