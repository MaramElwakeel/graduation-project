﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GP
{
    public struct ScaleData
    {
        public double ScaleFactor;
        public double Shift;

    };

    static public class Network
    {
        static public ScaleData ScaleData;
        static public List<double> SupportVectors = new List<double>();
        static public List<double> Alpha = new List<double>();
        static public double Bias;

        static public void Initialize(int Level)
        {
            if (Level == 0)
            {
                Bias = 1.392072607437320;

                //Set Scale Data Values
                ScaleData.ScaleFactor = 6.907541990992594;
                ScaleData.Shift = -0.596346131587186;

                //Set Alpha Values
                StreamReader SR = new StreamReader("Normal VS Epileptic Alpha Values.txt");
                string Value;
                while ((Value = SR.ReadLine()) != null)
                    Alpha.Add(double.Parse(Value));
                SR.Close();

                //Set Support Vectors Values
                SR = new StreamReader("Normal VS Epileptic SupportVectors Values.txt");
                while ((Value = SR.ReadLine()) != null)
                    SupportVectors.Add(double.Parse(Value));
                SR.Close();
            }
            else
            {
                Bias = 0.356227135701258;

                //Set Scale Data Values
                ScaleData.ScaleFactor = 10.303420562241293;
                ScaleData.Shift = -0.512244042885589;

                //Set Alpha Values
                StreamReader SR = new StreamReader("Ictal VS Interictal Alpha Values.txt");
                string Value;
                while ((Value = SR.ReadLine()) != null)
                    Alpha.Add(double.Parse(Value));
                SR.Close();

                //Set Support Vectors Values
                SR = new StreamReader("Ictal VS Interictal SupportVectors Values.txt");
                while ((Value = SR.ReadLine()) != null)
                    SupportVectors.Add(double.Parse(Value));
                SR.Close();
            }
        }
    }
}
