
scaleFactor = 6.907541990992594;
shift = -0.596346131587186;
Bias = 1.392072607437320;
test = [0.68138398009798, 0.58872712236413216, 0.68384442506086884, 0.723491206396468]

for i=1:4     %4 is here the number of features after concatenating channels
    X = test(:, i)
    test(:,i) = scaleFactor *(test(:,i) +  shift);% normalize data
end

res2=zeros(1,1);
for i=1:size(VarName1,1) %size betrg3 number of support vectors
    y = VarName1(i,:)
    z = test(1, :)
%     dotproduct = test(1,1) * y + test(1,2) * y + test(1,3) * y + test(1,4) * y 
%      res2(1)=res2(1)+ VarName2(i)*dotproduct %(test(1,:),VarName1(i,:));  %dot is dot product
     res2(1)=res2(1)+ dot(test(1,:),VarName1(i,:));  %dot is dot product

end
res2(1)=res2(1)+ Bias;

if res2(1)>=0 
    res2(1)=-1;   % thresholding
else
    res2(1)=1;
end