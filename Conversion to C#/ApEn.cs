﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GP
{
    public class ApEn
    {
        public double ApEn_fn(int Dimension, double r, List<double> Data)
        {
            List<double> Result = new List<double>();
            for (int c = 0; c < 2; c++)
            {
                int m = Dimension + c;
                int N = Data.Count;

                // Fill Matrix.
                List<List<double>> DataMatrix = new List<List<double>>();
                FillDataMatrix(N, m, Data, ref DataMatrix);

                // Operations.
                List<double> PhiList = new List<double>();
                for (int i = 0; i < N - m + 1; i++)
                {
                    double BoolVectorSum = 0, TempPhiSum = 0;

                    List<List<double>> RepeatedMatrix = new List<List<double>>();
                    for (int col = 0; col < m; col++)
                    {
                        List<double> sublist = new List<double>();
                        sublist.Add(DataMatrix[col][i]);
                        RepeatedMatrix.Add(sublist);
                    }
                    RepeatedMatrix = Repmat(RepeatedMatrix, m, N - m + 1);

                    CountingSimilarPatterns(DataMatrix, RepeatedMatrix, r, ref BoolVectorSum);
                    TempPhiSum = BoolVectorSum / (N - m + 1);
                    PhiList.Add(Math.Log(TempPhiSum));
                }
                double PhiSum = PhiList.Sum();
                Result.Add(PhiSum / (N - m + 1));
            } // End c

            double ApEn = 0;
            ApEn = Result[0] - Result[1];
            return ApEn;
        } // End ApEn

        List<List<double>> FillDataMatrix(int N, int m, List<double> Data, ref List<List<double>> DataMatrix)
        {
            for (int i = 0; i < m; i++)
            {
                List<double> sublist = new List<double>();
                for (int j = i; j < N - m + i + 1; j++)
                {
                    sublist.Add(Data[j]);
                }
                DataMatrix.Add(sublist);
            }
            return DataMatrix;
        }

        void CountingSimilarPatterns(List<List<double>> DataMatrix, List<List<double>> RepeatedMatrix, double r, ref double BoolVectorSum)
        {
            double[] BoolVector = new double[DataMatrix[0].Count];

            for (int i = 0; i < DataMatrix.Count; i++)
            {
                for (int j = 0; j < DataMatrix[i].Count; j++)
                {
                    double TempData = Math.Abs(DataMatrix[i][j] - RepeatedMatrix[i][j]);
                    if (TempData > r)
                    {
                        BoolVector[j] = 1;
                    }
                }
            }
            BoolVectorSum = BoolVector.Length - BoolVector.Sum(); // Sum of Zeros
        }



        // Helper Method.
        public List<List<double>> Repmat(List<List<double>> Matrix, int Rows, int Counter)
        {
            double[] array;
            for (int i = 0; i < Rows; i++)
            {
                array = new double[Matrix[i].Count];
                array = Matrix[i].ToArray();
                for (int j = 0; j < Counter - 1; j++)
                {
                    Matrix[i].AddRange(array);
                }
            }
            return Matrix;
        }

        double CalculatSTD(List<double> Vector)
        {
            double average = Vector.Average();
            double sumOfSquaresOfDifferences = Vector.Select(val => (val - average) * (val - average)).Sum();
            double SD = Math.Sqrt(sumOfSquaresOfDifferences / Vector.Count);
            return SD;
        }


    }
}
